package com.mgaudiofile.countries.model

import io.reactivex.Single
import retrofit2.http.GET

interface CountriesApi {

    @GET("DevTides/countries/master/countriesV2.json")  //a static file on github
    fun getCountries(): Single<List<Country>>                   //Single - emits one variable and then closes
}