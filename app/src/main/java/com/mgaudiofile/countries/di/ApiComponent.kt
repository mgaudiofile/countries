package com.mgaudiofile.countries.di

import com.mgaudiofile.countries.model.CountriesService
import com.mgaudiofile.countries.viewmodel.ListViewModel
import dagger.Component

@Component(modules = [ApiModule::class])
interface ApiComponent {

    fun inject(service: CountriesService)

    fun inject(viewModel: ListViewModel)
}