package com.mgaudiofile.countries.di

import com.mgaudiofile.countries.model.CountriesApi
import com.mgaudiofile.countries.model.CountriesService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ApiModule {

    private val BASE_URL = "https://raw.githubusercontent.com"

    @Provides
    fun provideCountriesApi(): CountriesApi {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())             //transform the Json to our data class object
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())      //transform the data class into an observable
            .build()
            .create(CountriesApi::class.java)
    }

    @Provides
    fun providesCountriesService(): CountriesService {
        return CountriesService()
    }
}